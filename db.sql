CREATE DATABASE taskdb;

CREATE TABLE task(
    id SERIAL,
    title VARCHAR(255) UNIQUE,
    description TEXT,
    PRIMARY KEY(id)
);