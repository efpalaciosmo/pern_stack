const { config } = require('dotenv');
config()

module.exports = {
    db: {
        user: process.env.db_user,
        password: process.env.db_password,
        host: process.env.db_host,
        port: process.env.db_port,
        database: process.env.database
    }
}