const { Router } = require('express');
const router = Router();

const { getTask, getTasks, createTask, updateTask, deleteTask } = require('../controllers/task.controller');

router.get('/task/:id', getTask);
router.get('/tasks', getTasks);
router.put('/task/:id', updateTask);
router.delete('/task/:id', deleteTask);
router.post('/task', createTask);

module.exports = router;