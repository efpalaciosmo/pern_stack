const pool = require('../db');

const createTask = async (req, res, next) =>{
    const { title, description } = req.body;
    try{
        const response = await pool.query('INSERT INTO task (title, description) VALUES ($1, $2)', [title, description]);
        res.status(200).json({
            message: 'user created'
        });
    } catch (error){
        next(error);
    }
};

const deleteTask = async (req, res, next) =>{
    try {
        const id = req.params.id;
        const response = await pool.query('DELETE FROM task WHERE id = $1', [id]);

        if (response.rowCount === 0){
            res.status(400).json({
                message: 'user not found'
            });
        }else{
            res.status(204).json({
                message: 'User deleted'
            });
        }
    } catch (error){
        next(error);
    }
}

const updateTask = async (req, res, next) =>{
    try {
        const id = req.params.id;
        const { title, description } = req.body;
        const response = await pool.query('UPDATE task SET title = $1, description = $2 WHERE id = $3 RETURNING *', [title, description, id]);

        if (response.rowCount === 0){
            res.status(400).json({
                message: 'user not found'
            });
        }else{
            res.status(200).json({
                message: 'User updated',
                'data': response.rows[0]
            });
        }
    } catch (error){
        next(error);
    }
}

const getTask = async (req, res, next) =>{
    try {
        const id = req.params.id;
        const response = await pool.query('SELECT * FROM task WHERE id = $1', [id]);

        if (response.rows === 0){
            res.status(400).json({
                message: 'User not found'
            });
        } else{
            res.status(200).json({
                message: 'User found',
                'data': response.rows[0]
            });
        }
    } catch (error){
        next(error);
    }
}

const getTasks = async (req, res, next) =>{
    try {
        const response = await pool.query('SELECT * FROM task');
        res.status(200).json({
            message: 'user created',
            'data': response.rows
        });
    } catch (error){
        next(error);
    }
}

module.exports = {
    getTask,
    getTasks,
    createTask,
    updateTask,
    deleteTask
}