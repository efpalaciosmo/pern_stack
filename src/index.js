const express = require('express');
const morgan = require('morgan');
const taskRoutes = require('./routes/task.routes');
const cors = require('cors');

const app = express();
app.use(morgan('dev'));
app.use(cors());
// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// routes
app.use(taskRoutes);

app.use((err, req, res, next) => {
    return res.json({
        message: err.message
    })
})

app.listen(3000);
console.log('Server on port 3000');